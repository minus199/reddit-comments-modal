const path = require('path'), fs = require('fs')
const glob = require('glob-all')
const _ = require('lodash')

const ignored = ['sizzle.min.js', 'jquery.fancytree-all-deps.min.js', 'jquery.fancytree-all.min.js']

const resolveVendorPaths = (depName, priority, realMain) => {

  const depMain = require.resolve(realMain || depName)
  const depRootParts = path.parse(depMain).dir.split(path.sep)//.filter(a => a.length)
  const resolvedDepRoot = _.take(depRootParts, depRootParts.lastIndexOf(depName) + 1).join(path.sep)

  function strategy1 (ext) { // Stricter
    const globOpts = {matchBase: true, cwd: resolvedDepRoot}
    const matches = glob.sync(['*', 'min', ext].join('.'), globOpts)

    return matches.
      filter(p => matches.length === 1 || path.basename(p.toLowerCase()).includes(depName.toLowerCase())).
      map(p => path.join(resolvedDepRoot, p))
  }

  function strategy2 (ext) {
    const globOpts = {matchBase: true, cwd: resolvedDepRoot}
    return glob.sync(['*', 'min', ext].join('.'), globOpts).
      map(p => path.join(resolvedDepRoot, p))
  }

  return [
    {ext: 'js', strategy: strategy1},
    {ext: 'css', strategy: strategy2},
  ].reduce((acc, {ext, strategy}) => {
    acc[ext] = strategy(ext)
    return acc
  }, {priority, depName})
}

function resolveVendor () {
  const {localMeta, dependencies} = require(process.env.PWD + '/package.json')

  const reduce = _.reduce(dependencies, (acc, depVersion, depName) => {
    const priority = Number.parseInt(localMeta.priorities[depName]) || Number.MAX_SAFE_INTEGER

    const resolved = resolveVendorPaths(depName, priority - 1, localMeta.realMain[depName])
    acc.push(resolved)

    return acc
  }, [])

  return _.sortBy(reduce, 'priority')
}

const main = () => {
  const {jsResolvedPaths, cssResolvedPaths} = resolveVendor().reduce((acc, resolved) => {
    acc.jsResolvedPaths.push(resolved.js)
    acc.cssResolvedPaths.push(resolved.css)
    return acc
  }, {jsResolvedPaths: [], cssResolvedPaths: []})

  cssResolvedPaths.push(glob.sync('./ext/styles/*'))
  jsResolvedPaths.push(glob.sync('./vendor/js/**/*.js'))

  return {
    cssResolvedPaths: _.flatten(cssResolvedPaths).filter(p => !ignored.includes(path.basename(p))),
    jsResolvedPaths: _.flatten(jsResolvedPaths).filter(p => !ignored.includes(path.basename(p))),
  }
}

function printDebug (output) {
  console.log('-'.repeat(50))
  // const s = _.map(output, (paths, groupName) => paths.map(p => path.relative('./node_modules', p))).join("\t\n")

  console.log(require("pretty-format")(output))
  console.log('-'.repeat(50))
}

module.exports = (debug = false) => {
  const output = main()
  debug || printDebug(output)
  return output
}
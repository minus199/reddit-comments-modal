const MAIN_DIR = process.env.PWD
const MAIN_DIST_DIR = module.exports.MAIN_DIST_DIR = MAIN_DIR + '/dist'

const LIB_DIR = module.exports.LIB_DIR = MAIN_DIST_DIR + '/ext/lib'
const ENTRY = module.exports.ENTRY = [MAIN_DIR + '/ext/lib/defaults/contentscript.js', MAIN_DIR + '/ext/lib/defaults/background.js']
const STATIC_SRC_DIR = MAIN_DIR + '/ext/static'

const cssResolvedPaths = module.exports.cssResolvedPaths = [
	MAIN_DIR + '/node_modules/jqueryui/jquery-ui.min.css',
	// './node_modules/jqueryui/jquery-ui.structure.min.css',
	MAIN_DIR + '/node_modules/jqueryui/jquery-ui.theme.min.css',
	// './node_modules/bootstrap/dist/css/bootstrap-theme.min.css',
	MAIN_DIR + '/node_modules/bootstrap/dist/css/bootstrap.min.css',
	
	MAIN_DIR + '/node_modules/jquery.fancytree/dist/skin-awesome/ui.fancytree.min.css',
	STATIC_SRC_DIR + '/styles/*.css'
]

const DIST = module.exports.DIST = {
	manifest: {from: MAIN_DIR + '/ext/static/manifest-firefox.json', to: MAIN_DIST_DIR, rename: 'manifest.json'},
	images: {from: [STATIC_SRC_DIR + '/images/**/*.{png,jpg}'], to: MAIN_DIST_DIR + '/images'},
	sprites: {from: [STATIC_SRC_DIR + '/images/**/*.*'], to: MAIN_DIST_DIR + '/images'},
	html: {from: [STATIC_SRC_DIR + '/html/**/*.*'], to: MAIN_DIST_DIR + '/html'},
	_locales: {from: [STATIC_SRC_DIR + '/_locales/**/*.*'], to: MAIN_DIST_DIR + '/_locales'},
	styles: {from: cssResolvedPaths, to: MAIN_DIST_DIR + '/styles'},
	fonts: {from: ['./node_modules/bootstrap/fonts/*'], to: MAIN_DIST_DIR + '/fonts'},
	templates: {from: STATIC_SRC_DIR + '/dust/', to: MAIN_DIST_DIR + '/templates.min.js'}
}

/**
 *
 * @type {{BUNDLE_EXT: string, BUNDLE_EXT_MAP: string, BUNDLE_VENDOR: string, BUNDLE_WRAPPED: string, BUNDLE_CSS: string}}
 */
const TARGETS = module.exports.TARGETS = {
	BUNDLE_EXT: MAIN_DIST_DIR + '/bundled.min.js',
	BUNDLE_EXT_MAP: MAIN_DIST_DIR + '/bundled.min.js.map',
	BUNDLE_VENDOR: MAIN_DIST_DIR + '/bundled-vendor.min.js',
	BUNDLE_WRAPPED: MAIN_DIST_DIR + '/bundled.wrapped.js',
	BUNDLE_CSS: DIST.styles.to + '/bundled.min.css',
	BUNDLE_TEMPLATES: DIST.templates.to + '/bundled.min.css'
}


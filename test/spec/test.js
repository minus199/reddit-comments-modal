const makeRequest = (resolve, reject) => {
  require('request')(
    {
      url: 'https://www.reddit.com/r/movies/comments/71joem/isle_of_dogs_wes_anderson_official_trailer_1/.json',
      headers: {
        accept: 'application/json',
        'User-Agent': 'com.minus.testing:devclient:1.0',
      },
      rejectUnauthorized: false,
    },
    (err, res, body) => err ? reject(err) : resolve(body))
}

const fs = require('fs'), resFileName = '../resources/res/res.json', isUpdate = false

return new Promise((resolve, reject) => {
  if (isUpdate === true) return makeRequest(resolve, reject)
  fs.readFile(resFileName, 'utf8', (err, data) => err ? reject(err) : resolve(JSON.parse(data)))
}).then(content => {
  if (isUpdate === true) fs.writeFile(resFileName, JSON.stringify(content, null, '\t'))

  const {root, things} = require("indica/core/entities/thing").builder(content)
  console.log('')
}).catch(console.error)

function IndicaLeaf () {
  this.isInstalled = false
}

IndicaLeaf.prototype = {
  kind: 'nothing',
  extractIndicaOpts () {
    throw Error('Must implement extractIndicaOpts')
  },

  installMetadataHoverToggle ($container) {
    if (this.isInstalled) return this
    const $metadataContainer = $container.find('.comment-metrics, .thing-meta-data').hide()

    const hideMetricsTimers = []
    $container.on('mouseover', e => {
      /* Clear any timeouts which were created on previous exit(s) effectively cancelling the hide command */
      if ($metadataContainer.css('display') === 'none') {
        hideMetricsTimers.forEach(clearTimeout)
        hideMetricsTimers.length = 0

        $metadataContainer.fadeIn()
      }
    }).on('mouseleave', e => {
      /*When leaving, it'll take the element [timeout] to hide. If after leaving(which sets timeout to hide), the user
      enters again, the hiding command will be cancelled and re created when next mouse leave*/
      hideMetricsTimers.push(setTimeout(() => $metadataContainer.fadeOut(), 1000))
    })

    this.isInstalled = true
    return this
  },
}

const RootIndicaLeaf = function () {}
RootIndicaLeaf.prototype = Object.create(IndicaLeaf.prototype)

/**
 *
 * @return {{
 * title: {thingURL, titleLink, titleText},
 * metrics, meta: {authorLink: string, authorName, subredditURL: string, subredditName: string, createdDate: number},
 * contents: {html},
 * preview: {thumbnail: *,srcOrig: string, hrefTN: string, thumbnailSrcURI: string},
 * expanded
 * }}
 */
RootIndicaLeaf.prototype.extractIndicaOpts = function () {
  const indicaOpts = {}

  indicaOpts.title = {
    titleLink: this.contents.attributes.url,
    titleText: this.contents.attributes.title,
    thingURL: this.contents.attributes.permalink,
  }

  indicaOpts.metrics = this.metrics.attributes
  indicaOpts.metrics.extraClass = 'comment-metrics'
  indicaOpts.meta = {
    authorLink: `/u/${this.author.attributes.author}`,
    authorName: this.author.attributes.author,
    subredditURL: `/r/${this.subreddit.attributes.subreddit}`,
    subredditName: this.subreddit.attributes.subreddit,
    createdDate: this.times.attributes.createdUtc,
  }

  indicaOpts.thumbnail = {
    thumbnailURL: this.contents.attributes.thumbnail,
  }

  indicaOpts.contents = {
    html: this.contents.attributes.selftextHtml,
  }

  if (this.media && this.media.attributes && this.media.attributes.secureMedia) {
    const media = this.media.attributes.secureMedia.oembed
    indicaOpts.expanded = {
      mediaType: media.type,

      providerType: this.media.attributes.secureMedia.type,
      providerUrl: media.provider_url,
      providerName: media.provider_name,

      title: media.title,

      authorUrl: media.author_url,
      authorName: media.author_name,

      thumbnailUrl: media.thumbnail_url,
      thumbnailWidth: media.thumbnail_width,
      thumbnailHeight: media.thumbnail_height,

      html: media.html,

      height: media.height,
      width: media.width,

      version: media.version,
    }
  }

  return indicaOpts
}

const CommentIndicaLeaf = function (replies) {
  this.replies = replies
}

CommentIndicaLeaf.prototype = Object.create(IndicaLeaf.prototype)

/**
 * @return {jQuery}
 */
CommentIndicaLeaf.prototype.toTreeNode = function toTreeNode () {
  const thingTemplate = require("indica/templates").leaf
  const $container = thingTemplate(this.extractIndicaOpts())

  try { // to be honest, mostly so it wont fail while running test no in browser // TODO FIX ME make better
    const $metadataContainer = $container.find('thing-meta-data').hide()
    this.installMetadataHoverToggle($container, $metadataContainer)
  } catch (e) {}

  return this.extractReplies($container)
}

CommentIndicaLeaf.prototype.extractReplies = function toTreeNode ($contentContainer) {
  // if (this.kind.toLowerCase() === 'more') return $('<div/>').addClass('more-replies-indicator').text('...')
  const numReplies = this.replies.length

  $contentContainer.
    addClass(`thing-with${numReplies === 0 ? 'out' : ''}-replies`).
    find('.md').removeClass('md').addClass('wasMD')

  if (numReplies > 0) {
    const treeNodes = this.replies.map(_thing => $('<li/>').append(_thing.toTreeNode()).addClass('thing-reply'))
    const $repliesContainer = $('<ul/>').addClass('replies-of-node').append(treeNodes)
    $contentContainer.find('#replies').replaceWith($repliesContainer)
  }

  return $contentContainer
}

/**
 * @return {{
 * contents,
 * metrics: {score, ups, downs, upvoteRatio, numComments, viewCount, numCrossposts, likes, gilded},
 * authorLink: string,
 * authorName,
 * createdDate: string}}
 */
CommentIndicaLeaf.prototype.extractIndicaOpts = function () {
  const metrics = this.scores.attributes
  metrics.extraClass = 'comment-metrics'

  return {
    contents: this.contents.attributes.bodyHtml,
    metrics,
    authorURL: `/user/${this.author.attributes.author}`,
    authorName: this.author.attributes.author,
    createdDate: this.times.attributes.createdUtc,
  }
}

module.exports = {RootIndicaLeaf, CommentIndicaLeaf}
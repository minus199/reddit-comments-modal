const ThingBuilder = require("indica/core/entities/thing").builder
const IndicaTree = require("indica/core/modal/IndicaTree")
const rootTemplate = require("indica/templates").root

function IndicaContentManager (mainContainerElement, dataProvider) {
  this.indicaDataProvider = dataProvider
  this.mainContainerElement = mainContainerElement
  this.indicaTree = new IndicaTree(this.mainContainerElement.find('#indica-comments-tree'))
}

IndicaContentManager.prototype.updateRoot = function (rootLeaf) {
  const currentRoot = rootTemplate(rootLeaf.extractIndicaOpts())
  const $root = this.mainContainerElement.find('#indica-tree-root').replaceWith(currentRoot)
  console.info(Date.now(), 'Replaced rootLeaf.')

  const {thumbnail} = rootLeaf.contents.attributes
  const showTN = !(!thumbnail || thumbnail.length === 0 || thumbnail === 'self')
  $root.find('#root-tn-container').toggle(showTN)

  return Promise.resolve($root)
}

IndicaContentManager.prototype.refresh = function (from) {
  return this.indicaDataProvider.provide(from).
    then(ThingBuilder).
    then(thingBundle => {
      const {rootLeaf, thingIndicaLeafs} = thingBundle
      this.updateRoot(rootLeaf)
      this.indicaTree.update(thingIndicaLeafs)
      $('.indica-thing-date').timeago()
      return thingBundle
    })
}

IndicaContentManager.prototype.install = function () {
  document.getElementById('indica').addEventListener('wheel', e => this.indicaTree.manageScrolling(e))
  this.indicaTree.installEventListeners()
  return this
}

module.exports = IndicaContentManager
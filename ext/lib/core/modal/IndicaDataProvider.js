function IndicaDataProvider () {}

IndicaDataProvider.prototype.preProvide = function (from) { console.info(`${Date.now()} [${__filename}] Provide from ${from}`) }

/**
 *
 * @param from url|path
 * @return {Promise}
 */
IndicaDataProvider.prototype.provide = function (from) { throw Error('Must implement provide.')}

function IndicaHttpDataProvider () {}

IndicaHttpDataProvider.prototype = Object.create(IndicaDataProvider.prototype)
IndicaHttpDataProvider.prototype.provide = function (from) {
  this.preProvide(from)
  return new Promise((resolve, reject) => require('https').get(from, res => resolve(res)))
}

function IndicaHttpDataProviderJquery () {}

IndicaHttpDataProviderJquery.prototype = Object.create(IndicaDataProvider.prototype)
IndicaHttpDataProviderJquery.prototype.provide = function (from) {
  this.preProvide(from)
  return new Promise((resolve, reject) => $.get(from, res => resolve(res)))
}

function IndicaFileDataProvider () {}

IndicaFileDataProvider.prototype = Object.create(IndicaDataProvider.prototype)
IndicaFileDataProvider.prototype.provide = function (from) {
  this.preProvide(from)
  return Promise.resolve(require(from))
}

module.exports = {
  http: IndicaHttpDataProvider,
  $: IndicaHttpDataProviderJquery,
  file: IndicaFileDataProvider,
}
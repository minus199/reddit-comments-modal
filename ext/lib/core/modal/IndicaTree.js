const IndicaTree = function IndicaTree (treeDomElement) {
  this.treeDomElement = treeDomElement
  this.vanillaElement = document.querySelector('#indica-comments-tree')
  console.info(Date.now(), `indicaModal was ${this.treeDomElement.length > 0 ? '' : 'NOT'} attached to body`)
}

/**
 *
 * @param thingIndicaLeafs
 * @return {{thingIndicaLeafs: *, childCount: {previousCount: {topLevel, all}, afterCount: {topLevel, all}}}}
 */
IndicaTree.prototype.update = function (thingIndicaLeafs) {
  const previousCount = this.childrenCount()

  this.treeDomElement.empty().append(thingIndicaLeafs.map(leaf => leaf.toTreeNode()))

  const afterCount = this.childrenCount()
  console.info(Date.now(), `Appended all things to dom.`,
    `Removed ${previousCount.topLevel}(total ${previousCount.all}) | added ${afterCount.top}(total${afterCount.all})`)

  return {thingIndicaLeafs, childCount: {previousCount, afterCount}}

}

IndicaTree.prototype.manageScrolling = function (e) {
  const isNotTreeNode = !this.vanillaElement.contains(e.target)
  const treeScrollPosition = this.vanillaElement.scrollTop
  const isTreeAtTop = e.deltaY < 0 && treeScrollPosition === 0
  const isTreeAtBottom = e.deltaY > 0 && treeScrollPosition + this.vanillaElement.clientHeight === this.vanillaElement.scrollHeight

  /*Stop scrolling*/
  if (isTreeAtTop || isNotTreeNode || isTreeAtBottom) {
    e.preventDefault()
    e.stopPropagation()
  }
}

IndicaTree.prototype.installEventListeners = function () {
  this.treeDomElement.on('click', '.thing-with-replies', function (e) {
    $(this).children('.replies-of-node').fadeToggle('fast')
    e.preventDefault()
    e.stopPropagation()
  })

  return this
}

/**
 *
 * @return {{topLevel, all}}
 */
IndicaTree.prototype.childrenCount = function () {
  return {
    topLevel: this.treeDomElement.children().length || 0,
    all: this.treeDomElement.find('.indica-comment-content-entry').length || 0,
  }
}

IndicaTree.prototype.isEmpty = function () {
  return this.childrenCount().topLevel === 0
}

module.exports = IndicaTree
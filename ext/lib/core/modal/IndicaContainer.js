const MINIMIZED = {right: '-660px', opacity: '0.9', height: '20%', top: '50%'}
const MAXIMIZED = {right: '0', opacity: '1', height: '100%', top: '0'}

const mainContainerTemplate = require('indica/templates').indica
const IndicaContentManager = require('indica/core/modal/IndicaContentManager')
const IndicaDataProvider = require('indica/core/modal/IndicaDataProvider').$

/**
 *
 * @type {module.IndicaContainer}
 */
module.exports = class IndicaContainer {
	constructor() {
		// since the element is attached to dom full size and visible, on container init the element is collapsed
		this.isBig = true
		this.isEmpty = true // indicates whether content were loaded into modal
		
		$('#indica').remove()
	}
	
	init() {
		mainContainerTemplate().then(element => {
			console.log('Container got rendered element', element)
			this.$element = $(element).appendTo($('body'))
			this.indicaContentManager = new IndicaContentManager(this.$element, new IndicaDataProvider())
			this.toggleButton = this.$element.find('#indica-toggle-root')
			this.toggleButton.text('-x-')
			
			const expando = require('expando')
			console.info(`${Date.now()} [${__filename}] - Indica is being installed...`)
			this.minimize()
			
			this.indicaContentManager.install()
			const $previewImage = this.$element.find('#root-original-image-container')
			this.$element.find('#root-thumbnail-link').click(function (e) {
				if ($previewImage.hasClass('image-expanded')) {
					expando.collapse($previewImage, () => $previewImage.removeClass('image-expanded'))
				} else {
					expando.expand($previewImage, () => $previewImage.addClass('image-expanded'))
				}
				
				e.preventDefault()
			})
			
			this.toggleButton.click(() => this.toggle())
			
			console.info(`${Date.now()} [${__filename}] - Indica is up.`)
		}).catch(err => {
			console.log('Error while initializing main container', err)
		})
	}
	
	/**
	 *
	 * @param from
	 * @return {Promise}
	 */
	refresh(from) {
		console.info(`${Date.now()} [${__filename}] Trigger click detected - Refreshing di Indica from ${from}`)
		
		return this.indicaContentManager.refresh(from).then(() => {
			this.isEmpty = false
			this.maximize()
			console.info(Date.now(), 'Done updating.')
		})
	}
	
	maximize() {
		return this.toggle(true, () => this.$element.removeAttr('style'))
	}
	
	minimize() {
		return this.toggle(false)
	}
	
	toggle(maximize, cb) {
		if (this.isEmpty && !this.isBig) { // when toggling big but there's no content inside the modal
			return this
		}
		
		// Either a state was forced via arg, or toggling to opposite state if no forced state was specified
		const toggleTo = maximize !== undefined ? maximize : !this.isBig
		
		// when the current state is the same as the desired state
		if (this.isBig === maximize) return this
		
		this.$element.animate(toggleTo ? MAXIMIZED : MINIMIZED, () => {
			this.isBig = toggleTo
			this.toggleButton.text(this.isBig ? 'X' : 'O')
			if (typeof cb === 'function') cb.bind(this)()
		})
		
		return this
	}
}
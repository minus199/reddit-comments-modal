require('harmony-reflect')

/**
 * @access private
 * @extends {Proxy}
 */
class ThingProxy extends Proxy {
  /**
   * @param {Thing} [thingInstance]
   */
  constructor (thingInstance) {
    const handler = {
      get: (target, name) => {
        return name in target ? target[name] : target.get(name)
      },

      set: (target, name, value, receiver) => {
        if (name in target) {
          throw new Error(`Cannot define a service named ${name}`)
        }

        target.share(name, value)

        return true
      },

      has: (target, name) => target.exists(name),

      deleteProperty: () => false,
    }

    super(thingInstance, handler)
  }
}

module.exports = ThingProxy
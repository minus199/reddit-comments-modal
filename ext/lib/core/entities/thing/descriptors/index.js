/**
 * @constructor
 * @typedef {Object} ThingBundle
 * @param {RootThing} root
 * @param {Array.<CommentThing>} things
 * @member {RootThing} ThingBundle.root The root Thing.
 * @member {Array.<CommentThing>} ThingBundle.things Nested children things
 */
function ThingBundle (root, things) {
  this.root = root
  this.things = things
}

module.exports = {
  ThingDesc: require("indica/core/entities/thing/descriptors/ThingDescriptor"),
  RootThingDesc: require("indica/core/entities/thing/descriptors/RootThingDescriptor"),
  ThingBundle
}
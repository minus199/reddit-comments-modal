module.exports = Object.freeze([
  {
    'attrName': 'id',
    'section': 'thingMeta',
  },
  {
    'attrName': 'name',
    'section': 'thingMeta',
  },
  {
    'attrName': 'parent_whitelist_status',
    'section': 'whitelist',
  },
  {
    'attrName': 'whitelist_status',
    'section': 'whitelist',
  },
  {
    'attrName': 'author',
    'section': 'author',
  },
  {
    'attrName': 'author_flair_text',
    'section': 'author',
  },
  {
    'attrName': 'author_flair_css_class',
    'section': 'author',
  },
  {
    'attrName': 'subreddit',
    'section': 'Subreddit',
  },
  {
    'attrName': 'domain',
    'section': 'Subreddit',
  },
  {
    'attrName': 'subreddit_id',
    'section': 'Subreddit',
  },
  {
    'attrName': 'subreddit_name_prefixed',
    'section': 'Subreddit',
  },
  {
    'attrName': 'subreddit_type',
    'section': 'Subreddit',
  },
  {
    'attrName': 'created',
    'section': 'times',
  },
  {
    'attrName': 'created_utc',
    'section': 'times',
  },
  {
    'attrName': 'approved_at_utc',
    'section': 'times',
  },
  {
    'attrName': 'banned_at_utc',
    'section': 'times',
  },
  {
    'attrName': 'media',
    'section': 'media',
  },
  {
    'attrName': 'media_embed',
    'section': 'media',
  },
  {
    'attrName': 'secure_media',
    'section': 'media',
  },
  {
    'attrName': 'secure_media_embed',
    'section': 'media',
  },
  {
    'attrName': 'link_flair_css_class',
    'section': 'flairs',
  },
  {
    'attrName': 'link_flair_text',
    'section': 'flairs',
  },
  {
    'attrName': 'url',
    'section': 'content',
  },
  {
    'attrName': 'permalink',
    'section': 'content',
  },
  {
    'attrName': 'title',
    'section': 'content',
  },
  {
    'attrName': 'selftext_html',
    'section': 'content',
  },
  {
    'attrName': 'selftext',
    'section': 'content',
  },
  {
    'attrName': 'thumbnail',
    'section': 'content',
  },
  {
    'attrName': 'thumbnail_height',
    'section': 'content',
  },
  {
    'attrName': 'thumbnail_width',
    'section': 'content',
  },
  {
    'attrName': 'suggested_sort',
    'section': 'content',
  },
  {
    'attrName': 'score',
    'section': 'metrics',
    'html': true,
  },
  {
    'attrName': 'ups',
    'section': 'metrics',
  },
  {
    'attrName': 'downs',
    'section': 'metrics',
  },
  {
    'attrName': 'upvote_ratio',
    'section': 'metrics',
    'html': true,
  },
  {
    'attrName': 'num_comments',
    'section': 'metrics',
    'html': true,
  },
  {
    'attrName': 'view_count',
    'section': 'metrics',
  },
  {
    'attrName': 'num_crossposts',
    'section': 'metrics',
    'html': true,
  },
  {
    'attrName': 'likes',
    'section': 'metrics',
  },
  {
    'attrName': 'gilded',
    'section': 'metrics',
  },
  {
    'attrName': 'clicked',
    'section': 'user_state',
  },
  {
    'attrName': 'visited',
    'section': 'user_state',
  },
  {
    'attrName': 'saved',
    'section': 'user_state',
  },
  {
    'attrName': 'spoiler',
    'section': 'flags',
  },
  {
    'attrName': 'pinned',
    'section': 'flags',
  },
  {
    'attrName': 'over_18',
    'section': 'flags',
  },
  {
    'attrName': 'archived',
    'section': 'flags',
  },
  {
    'attrName': 'contest_mode',
    'section': 'flags',
  },
  {
    'attrName': 'brand_safe',
    'section': 'flags',
  },
  {
    'attrName': 'edited',
    'section': 'flags',
  },
  {
    'attrName': 'is_crosspostable',
    'section': 'flags',
  },
  {
    'attrName': 'hidden',
    'section': 'flags',
  },
  {
    'attrName': 'is_self',
    'section': 'flags',
  },
  {
    'attrName': 'is_video',
    'section': 'flags',
  },
  {
    'attrName': 'hide_score',
    'section': 'global_state',
  },
  {
    'attrName': 'approved_by',
    'section': 'global_state',
  },
  {
    'attrName': 'stickied',
    'section': 'global_state',
  },
  {
    'attrName': 'locked',
    'section': 'global_state',
  },
  {
    'attrName': 'quarantine',
    'section': 'global_state',
  },
  {
    'attrName': 'distinguished',
    'section': 'global_state',
  },
  {
    'attrName': 'can_gild',
    'section': 'global_state',
  },
  {
    'attrName': 'user_reports',
    'section': 'mod',
  },
  {
    'attrName': 'num_reports',
    'section': 'mod',
  },
  {
    'attrName': 'report_reasons',
    'section': 'mod',
  },
  {
    'attrName': 'mod_reports',
    'section': 'mod',
  },
  {
    'attrName': 'can_mod_post',
    'section': 'mod',
  },
  {
    'attrName': 'removal_reason',
    'section': 'mod',
  },
  {
    'attrName': 'banned_by',
    'section': 'mod',
  },
])
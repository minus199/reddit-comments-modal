module.exports = Object.freeze([
  {
    "attrName": "link_id",
    "section": "meta"
  },
  {
    "attrName": "id",
    "section": "meta"
  },
  {
    "attrName": "parent_id",
    "section": "meta"
  },
  {
    "attrName": "name",
    "section": "meta"
  },
  {
    "attrName": "depth",
    "section": "meta"
  },
  {
    "attrName": "likes",
    "section": "activity"
  },
  {
    "attrName": "replies",
    "section": "activity"
  },
  {
    "attrName": "user_reports",
    "section": "activity"
  },
  {
    "attrName": "mod_reports",
    "section": "activity"
  },
  {
    "attrName": "num_reports",
    "section": "activity"
  },
  {
    "attrName": "gilded",
    "section": "scores"
  },
  {
    "attrName": "ups",
    "section": "scores"
  },
  {
    "attrName": "score",
    "section": "scores"
  },
  {
    "attrName": "downs",
    "section": "scores"
  },
  {
    "attrName": "score_hidden",
    "section": "scores"
  },
  {
    "attrName": "controversiality",
    "section": "scores"
  },
  {
    "attrName": "author",
    "section": "author"
  },
  {
    "attrName": "is_submitter",
    "section": "author"
  },
  {
    "attrName": "author_flair_text",
    "section": "author"
  },
  {
    "attrName": "author_flair_css_class",
    "section": "author"
  },
  {
    "attrName": "saved",
    "section": "flags"
  },
  {
    "attrName": "distinguished",
    "section": "flags"
  },
  {
    "attrName": "archived",
    "section": "flags"
  },
  {
    "attrName": "edited",
    "section": "flags"
  },
  {
    "attrName": "stickied",
    "section": "flags"
  },
  {
    "attrName": "can_gild",
    "section": "flags"
  },
  {
    "attrName": "subreddit_id",
    "section": "subreddit"
  },
  {
    "attrName": "subreddit_name_prefixed",
    "section": "subreddit"
  },
  {
    "attrName": "subreddit",
    "section": "subreddit"
  },
  {
    "attrName": "subreddit_type",
    "section": "subreddit"
  },
  {
    "attrName": "created",
    "section": "times"
  },
  {
    "attrName": "created_utc",
    "section": "times"
  },
  {
    "attrName": "approved_at_utc",
    "section": "times"
  },
  {
    "attrName": "banned_at_utc",
    "section": "times"
  },
  {
    "attrName": "can_mod_post",
    "section": "mod"
  },
  {
    "attrName": "approved_by",
    "section": "mod"
  },
  {
    "attrName": "banned_by",
    "section": "mod"
  },
  {
    "attrName": "removal_reason",
    "section": "mod"
  },
  {
    "attrName": "report_reasons",
    "section": "mod"
  },
  {
    "attrName": "body",
    "section": "content"
  },
  {
    "attrName": "body_html",
    "section": "content"
  },
  {
    "attrName": "collapsed",
    "section": "view"
  },
  {
    "attrName": "collapsed_reason",
    "section": "view"
  }
])
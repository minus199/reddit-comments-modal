const noop = {execOnMatch: () => {}}

const Kind = function Kind (rawKind) {
  this.raw = rawKind
  this.wasExec = false
  this.wasMatched = false
}

Kind.prototype.is = function (kind) {
  this.wasMatched = kind === this.raw
  return this.wasMatched ? this : noop
}

Kind.prototype.execOnMatch = function (cb, breakChain) {
  if (this.isKnown(this.raw)) {
    this.wasExec = true
    cb()
    breakChain()
  }
}

Kind.prototype.isKnown = function (kind) { return ['Listing', 't1', 'more'].includes(kind) }

Kind.prototype.validate = function () {
  return new Promise((rez, rej) => this.isKnown(this.raw) ? rez(this) : rej(this))
}

/**
 *
 * @type {Array<Kind>}
 */
module.exports.unknown = []

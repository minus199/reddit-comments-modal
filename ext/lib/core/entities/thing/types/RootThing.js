function RootThing () {}

/**
 * @constructor
 * @typedef {Object} ThingMeta
 * @member ThingMeta.id,* @member ThingMeta.name
 */
RootThing.prototype.ThingMeta = function (id, name) {
  this.typeName = 'ThingMeta'
  this.attributes = {
    id: id,
    name: name,
  }
}
RootThing.prototype.ThingMeta.rawKeyNames = ['id', 'name']
RootThing.prototype.ThingMeta.displayAttributes = []

/**
 * @constructor
 * @typedef {Object} Whitelist
 * @member Whitelist.parentWhitelistStatus,* @member Whitelist.whitelistStatus
 */
RootThing.prototype.Whitelist = function (parent_whitelist_status, whitelist_status) {
  this.typeName = 'Whitelist'
  this.attributes = {
    parentWhitelistStatus: parent_whitelist_status,
    whitelistStatus: whitelist_status,
  }
}
RootThing.prototype.Whitelist.rawKeyNames = ['parent_whitelist_status', 'whitelist_status']
RootThing.prototype.Whitelist.displayAttributes = []

/**
 * @constructor
 * @typedef {Object} Author
 * @member Author.author,* @member Author.authorFlairText,* @member Author.authorFlairCssClass
 */
RootThing.prototype.Author = function (author, author_flair_text, author_flair_css_class) {
  this.typeName = 'Author'
  this.attributes = {
    author: author,
    authorFlairText: author_flair_text,
    authorFlairCssClass: author_flair_css_class,
  }
}
RootThing.prototype.Author.rawKeyNames = ['author', 'author_flair_text', 'author_flair_css_class']
RootThing.prototype.Author.displayAttributes = []

/**
 * @constructor
 * @typedef {Object} Subreddit
 * @member Subreddit.subreddit,* @member Subreddit.domain,* @member Subreddit.subredditId,* @member Subreddit.subredditNamePrefixed,* @member Subreddit.subredditType
 */
RootThing.prototype.Subreddit = function (subreddit, domain, subreddit_id, subreddit_name_prefixed, subreddit_type) {
  this.typeName = 'Subreddit'
  this.attributes = {
    subreddit: subreddit,
    domain: domain,
    subredditId: subreddit_id,
    subredditNamePrefixed: subreddit_name_prefixed,
    subredditType: subreddit_type,
  }
}
RootThing.prototype.Subreddit.rawKeyNames = ['subreddit', 'domain', 'subreddit_id', 'subreddit_name_prefixed', 'subreddit_type']
RootThing.prototype.Subreddit.displayAttributes = []

/**
 * @constructor
 * @typedef {Object} Times
 * @member Times.created,* @member Times.createdUtc,* @member Times.approvedAtUtc,* @member Times.bannedAtUtc
 */
RootThing.prototype.Times = function (created, created_utc, approved_at_utc, banned_at_utc) {
  this.typeName = 'Times'
  this.attributes = {
    created: created,
    createdUtc: created_utc,
    approvedAtUtc: approved_at_utc,
    bannedAtUtc: banned_at_utc,
  }
}
RootThing.prototype.Times.rawKeyNames = ['created', 'created_utc', 'approved_at_utc', 'banned_at_utc']
RootThing.prototype.Times.displayAttributes = []

/**
 * @constructor
 * @typedef {Object} Media
 * @member Media.media,* @member Media.mediaEmbed,* @member Media.secureMedia,* @member Media.secureMediaEmbed
 */
RootThing.prototype.Media = function (media, media_embed, secure_media, secure_media_embed) {
  this.typeName = 'Media'
  this.attributes = {
    media: media,
    mediaEmbed: media_embed,
    secureMedia: secure_media,
    secureMediaEmbed: secure_media_embed,
  }
}
RootThing.prototype.Media.rawKeyNames = ['media', 'media_embed', 'secure_media', 'secure_media_embed']
RootThing.prototype.Media.displayAttributes = []

/**
 * @constructor
 * @typedef {Object} Flairs
 * @member Flairs.linkFlairCssClass,* @member Flairs.linkFlairText
 */
RootThing.prototype.Flairs = function (link_flair_css_class, link_flair_text) {
  this.typeName = 'Flairs'
  this.attributes = {
    linkFlairCssClass: link_flair_css_class,
    linkFlairText: link_flair_text,
  }
}
RootThing.prototype.Flairs.rawKeyNames = ['link_flair_css_class', 'link_flair_text']
RootThing.prototype.Flairs.displayAttributes = []

/**
 * @constructor
 * @typedef {Object} Content
 * @member Content.url,* @member Content.permalink,* @member Content.title,* @member Content.selftextHtml,* @member Content.selftext,* @member Content.thumbnail,* @member Content.thumbnailHeight,* @member Content.thumbnailWidth,* @member Content.suggestedSort
 */
RootThing.prototype.Contents = function (
  url, permalink, title, selftext_html, selftext, thumbnail, thumbnail_height, thumbnail_width, suggested_sort) {
  this.typeName = 'Contents'
  this.attributes = {
    url: url,
    permalink: permalink,
    title: title,
    selftextHtml: selftext_html,
    selftext: selftext,
    thumbnail: thumbnail,
    thumbnailHeight: thumbnail_height,
    thumbnailWidth: thumbnail_width,
    suggestedSort: suggested_sort,
  }
}
RootThing.prototype.Contents.rawKeyNames = [
  'url',
  'permalink',
  'title',
  'selftext_html',
  'selftext',
  'thumbnail',
  'thumbnail_height',
  'thumbnail_width',
  'suggested_sort']
RootThing.prototype.Contents.displayAttributes = []

/**
 * @constructor
 * @typedef {Object} Metrics
 * @member Metrics.score,* @member Metrics.ups,* @member Metrics.downs,* @member Metrics.upvoteRatio,* @member Metrics.numComments,* @member Metrics.viewCount,* @member Metrics.numCrossposts,* @member Metrics.likes,* @member Metrics.gilded
 */
RootThing.prototype.Metrics = function (score, ups, downs, upvote_ratio, num_comments, view_count, num_crossposts, likes, gilded) {
  this.typeName = 'Metrics'
  this.attributes = {
    score: score,
    ups: ups,
    downs: downs,
    upvoteRatio: upvote_ratio,
    numComments: num_comments,
    viewCount: view_count,
    numCrossposts: num_crossposts,
    likes: likes,
    gilded: gilded,
  }
}
RootThing.prototype.Metrics.rawKeyNames = ['score', 'ups', 'downs', 'upvote_ratio', 'num_comments', 'view_count', 'num_crossposts', 'likes', 'gilded']
RootThing.prototype.Metrics.displayAttributes = ['score', 'upvote_ratio', 'num_comments', 'num_crossposts']

/**
 * @constructor
 * @typedef {Object} UserState
 * @member UserState.clicked,* @member UserState.visited,* @member UserState.saved
 */
RootThing.prototype.UserState = function (clicked, visited, saved) {
  this.typeName = 'UserState'
  this.attributes = {
    clicked: clicked,
    visited: visited,
    saved: saved,
  }
}
RootThing.prototype.UserState.rawKeyNames = ['clicked', 'visited', 'saved']
RootThing.prototype.UserState.displayAttributes = []

/**
 * @constructor
 * @typedef {Object} Flags
 * @member Flags.spoiler,* @member Flags.pinned,* @member Flags.over18,* @member Flags.archived,* @member Flags.contestMode,* @member Flags.brandSafe,* @member Flags.edited,* @member Flags.isCrosspostable,* @member Flags.hidden,* @member Flags.isSelf,* @member Flags.isVideo
 */
RootThing.prototype.Flags = function (
  spoiler, pinned, over_18, archived, contest_mode, brand_safe, edited, is_crosspostable, hidden, is_self, is_video) {
  this.typeName = 'Flags'
  this.attributes = {
    spoiler: spoiler,
    pinned: pinned,
    over18: over_18,
    archived: archived,
    contestMode: contest_mode,
    brandSafe: brand_safe,
    edited: edited,
    isCrosspostable: is_crosspostable,
    hidden: hidden,
    isSelf: is_self,
    isVideo: is_video,
  }
}
RootThing.prototype.Flags.rawKeyNames = [
  'spoiler',
  'pinned',
  'over_18',
  'archived',
  'contest_mode',
  'brand_safe',
  'edited',
  'is_crosspostable',
  'hidden',
  'is_self',
  'is_video']
RootThing.prototype.Flags.displayAttributes = []

/**
 * @constructor
 * @typedef {Object} GlobalState
 * @member GlobalState.hideScore,* @member GlobalState.approvedBy,* @member GlobalState.stickied,* @member GlobalState.locked,* @member GlobalState.quarantine,* @member GlobalState.distinguished,* @member GlobalState.canGild
 */
RootThing.prototype.GlobalState = function (hide_score, approved_by, stickied, locked, quarantine, distinguished, can_gild) {
  this.typeName = 'GlobalState'
  this.attributes = {
    hideScore: hide_score,
    approvedBy: approved_by,
    stickied: stickied,
    locked: locked,
    quarantine: quarantine,
    distinguished: distinguished,
    canGild: can_gild,
  }
}
RootThing.prototype.GlobalState.rawKeyNames = ['hide_score', 'approved_by', 'stickied', 'locked', 'quarantine', 'distinguished', 'can_gild']
RootThing.prototype.GlobalState.displayAttributes = []

/**
 * @constructor
 * @typedef {Object} Mod
 * @member Mod.userReports,* @member Mod.numReports,* @member Mod.reportReasons,* @member Mod.modReports,* @member Mod.canModPost,* @member Mod.removalReason,* @member Mod.bannedBy
 */
RootThing.prototype.Mod = function (user_reports, num_reports, report_reasons, mod_reports, can_mod_post, removal_reason, banned_by) {
  this.typeName = 'Mod'
  this.attributes = {
    userReports: user_reports,
    numReports: num_reports,
    reportReasons: report_reasons,
    modReports: mod_reports,
    canModPost: can_mod_post,
    removalReason: removal_reason,
    bannedBy: banned_by,
  }
}
RootThing.prototype.Mod.rawKeyNames = ['user_reports', 'num_reports', 'report_reasons', 'mod_reports', 'can_mod_post', 'removal_reason', 'banned_by']
RootThing.prototype.Mod.displayAttributes = []

function newCall (Cls, rawThing) {
  const extractedArgs = Cls.rawKeyNames.reduce((acc, k) => {
    if (Object.keys(rawThing).includes(k)) {
      if (k === 'replies') {
        // const children = _.get(rawThing, 'replies.data.children')
        // recurse
      }

      acc.boundArgs.push(rawThing[k])
      acc.dictArgs.push({name: k, val: rawThing[k]})
      return acc
    }

    acc.errors.push({name: k, val: undefined, msg: `No key ${k} found in comment thing`, rawThing, keys: Cls.rawKeyNames})

    return acc
  }, {boundArgs: [null], dictArgs: [], errors: []})

  const Constructor = Function.prototype.bind.apply(Cls, extractedArgs.boundArgs)
  return new Constructor()
}

/**
 * @constructor
 * @typedef {Object} %s', RootThing
 * @member {ThingMeta} RootThing.thingMeta
 * @member {Whitelist} RootThing.whitelist
 * @member {Author} RootThing.author
 * @member {Subreddit} RootThing.subreddit
 * @member {Times} RootThing.times
 * @member {Media} RootThing.media
 * @member {Flairs} RootThing.flairs
 * @member {Content} RootThing.content
 * @member {Metrics} RootThing.metrics
 * @member {UserState} RootThing.userState
 * @member {Flags} RootThing.flags
 * @member {GlobalState} RootThing.globalState
 * @member {Mod} RootThing.mod
 */
module.exports = function (rawThing) {
  this.raw = () => rawThing
  this.raw.helperFunc = true

  this.thingMeta = newCall(RootThing.prototype.ThingMeta, rawThing)
  this.whitelist = newCall(RootThing.prototype.Whitelist, rawThing)
  this.author = newCall(RootThing.prototype.Author, rawThing)
  this.subreddit = newCall(RootThing.prototype.Subreddit, rawThing)
  this.times = newCall(RootThing.prototype.Times, rawThing)
  this.media = newCall(RootThing.prototype.Media, rawThing)
  this.flairs = newCall(RootThing.prototype.Flairs, rawThing)
  this.contents = newCall(RootThing.prototype.Contents, rawThing)
  this.metrics = newCall(RootThing.prototype.Metrics, rawThing)
  this.userState = newCall(RootThing.prototype.UserState, rawThing)
  this.flags = newCall(RootThing.prototype.Flags, rawThing)
  this.globalState = newCall(RootThing.prototype.GlobalState, rawThing)
  this.mod = newCall(RootThing.prototype.Mod, rawThing)
}
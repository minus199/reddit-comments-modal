function CommentThing () {}

/**
 * @constructor
 * @typedef {Object} Meta
 * @member Meta.linkId,* @member Meta.id,* @member Meta.parentId,* @member Meta.name,* @member Meta.depth
 */
CommentThing.prototype.Meta = function (link_id, id, parent_id, name, depth) {
  this.typeName = 'Meta'
  this.attributes = {
    linkId: link_id,
    id: id,
    parentId: parent_id,
    name: name,
    depth: depth,
  }
}
CommentThing.prototype.Meta.rawKeyNames = ['link_id', 'id', 'parent_id', 'name', 'depth']
CommentThing.prototype.Meta.displayAttributes = []

/**
 * @constructor
 * @typedef {Object} Activity
 * @member Activity.likes,* @member Activity.replies,* @member Activity.userReports,* @member Activity.modReports,* @member Activity.numReports
 */
CommentThing.prototype.Activity = function (likes, replies, user_reports, mod_reports, num_reports) {
  this.typeName = 'Activity'
  this.attributes = {
    likes: likes,
    replies: replies,
    userReports: user_reports,
    modReports: mod_reports,
    numReports: num_reports,
  }
}
CommentThing.prototype.Activity.rawKeyNames = ['likes', 'replies', 'user_reports', 'mod_reports', 'num_reports']
CommentThing.prototype.Activity.displayAttributes = []

/**
 * @constructor
 * @typedef {Object} Scores
 * @member Scores.gilded,* @member Scores.ups,* @member Scores.score,* @member Scores.downs,* @member Scores.scoreHidden,* @member Scores.controversiality
 */
CommentThing.prototype.Scores = function (gilded, ups, score, downs, score_hidden, controversiality) {
  this.typeName = 'Scores'
  this.attributes = {
    gilded: gilded,
    ups: ups,
    score: score,
    downs: downs,
    scoreHidden: score_hidden,
    controversiality: controversiality,
  }
}
CommentThing.prototype.Scores.rawKeyNames = ['gilded', 'ups', 'score', 'downs', 'score_hidden', 'controversiality']
CommentThing.prototype.Scores.displayAttributes = []

/**
 * @constructor
 * @typedef {Object} Author
 * @member Author.author,* @member Author.isSubmitter,* @member Author.authorFlairText,* @member Author.authorFlairCssClass
 */
CommentThing.prototype.Author = function (author, is_submitter, author_flair_text, author_flair_css_class) {
  this.typeName = 'Author'
  this.attributes = {
    author: author,
    isSubmitter: is_submitter,
    authorFlairText: author_flair_text,
    authorFlairCssClass: author_flair_css_class,
  }
}
CommentThing.prototype.Author.rawKeyNames = ['author', 'is_submitter', 'author_flair_text', 'author_flair_css_class']
CommentThing.prototype.Author.displayAttributes = []

/**
 * @constructor
 * @typedef {Object} Flags
 * @member Flags.saved,* @member Flags.distinguished,* @member Flags.archived,* @member Flags.edited,* @member Flags.stickied,* @member Flags.canGild
 */
CommentThing.prototype.Flags = function (saved, distinguished, archived, edited, stickied, can_gild) {
  this.typeName = 'Flags'
  this.attributes = {
    saved: saved,
    distinguished: distinguished,
    archived: archived,
    edited: edited,
    stickied: stickied,
    canGild: can_gild,
  }
}
CommentThing.prototype.Flags.rawKeyNames = ['saved', 'distinguished', 'archived', 'edited', 'stickied', 'can_gild']
CommentThing.prototype.Flags.displayAttributes = []

/**
 * @constructor
 * @typedef {Object} Subreddit
 * @member Subreddit.subredditId,* @member Subreddit.subredditNamePrefixed,* @member Subreddit.subreddit,* @member Subreddit.subredditType
 */
CommentThing.prototype.Subreddit = function (subreddit_id, subreddit_name_prefixed, subreddit, subreddit_type) {
  this.typeName = 'Subreddit'
  this.attributes = {
    subredditId: subreddit_id,
    subredditNamePrefixed: subreddit_name_prefixed,
    subreddit: subreddit,
    subredditType: subreddit_type,
  }
}
CommentThing.prototype.Subreddit.rawKeyNames = ['subreddit_id', 'subreddit_name_prefixed', 'subreddit', 'subreddit_type']
CommentThing.prototype.Subreddit.displayAttributes = []

/**
 * @constructor
 * @typedef {Object} Times
 * @member Times.created,* @member Times.createdUtc,* @member Times.approvedAtUtc,* @member Times.bannedAtUtc
 */
CommentThing.prototype.Times = function (created, created_utc, approved_at_utc, banned_at_utc) {
  this.typeName = 'Times'
  this.attributes = {
    created: created,
    createdUtc: created_utc,
    approvedAtUtc: approved_at_utc,
    bannedAtUtc: banned_at_utc,
  }
}
CommentThing.prototype.Times.rawKeyNames = ['created', 'created_utc', 'approved_at_utc', 'banned_at_utc']
CommentThing.prototype.Times.displayAttributes = []

/**
 * @constructor
 * @typedef {Object} Mod
 * @member Mod.canModPost,* @member Mod.approvedBy,* @member Mod.bannedBy,* @member Mod.removalReason,* @member Mod.reportReasons
 */
CommentThing.prototype.Mod = function (can_mod_post, approved_by, banned_by, removal_reason, report_reasons) {
  this.typeName = 'Mod'
  this.attributes = {
    canModPost: can_mod_post,
    approvedBy: approved_by,
    bannedBy: banned_by,
    removalReason: removal_reason,
    reportReasons: report_reasons,
  }
}
CommentThing.prototype.Mod.rawKeyNames = ['can_mod_post', 'approved_by', 'banned_by', 'removal_reason', 'report_reasons']
CommentThing.prototype.Mod.displayAttributes = []

/**
 * @constructor
 * @typedef {Object} Content
 * @member Content.body,* @member Content.bodyHtml
 */
CommentThing.prototype.Contents = function (body, body_html) {
  this.typeName = 'Content'
  this.attributes = {
    body: body,
    bodyHtml: body_html,
  }
}
CommentThing.prototype.Contents.rawKeyNames = ['body', 'body_html']
CommentThing.prototype.Contents.displayAttributes = []

/**
 * @constructor
 * @typedef {Object} View
 * @member View.collapsed,* @member View.collapsedReason
 */
CommentThing.prototype.View = function (collapsed, collapsed_reason) {
  this.typeName = 'View'
  this.attributes = {
    collapsed: collapsed,
    collapsedReason: collapsed_reason,
  }
}
CommentThing.prototype.View.rawKeyNames = ['collapsed', 'collapsed_reason']
CommentThing.prototype.View.displayAttributes = []

function newCall (Cls, rawThing) {
  const extractedArgs = Cls.rawKeyNames.reduce((acc, k) => {
    if (Object.keys(rawThing).includes(k)) {
      if (k === 'replies') {
        // const children = _.get(rawThing, 'replies.data.children')
        // recurse
      }

      acc.boundArgs.push(rawThing[k])
      acc.dictArgs.push({name: k, val: rawThing[k]})
      return acc
    }

    acc.errors.push({name: k, val: undefined, msg: `No key ${k} found in comment thing`, rawThing, keys: Cls.rawKeyNames})

    return acc
  }, {boundArgs: [null], dictArgs: [], errors: []})

  const Constructor = Function.prototype.bind.apply(Cls, extractedArgs.boundArgs)
  return new Constructor()
}

/**
 * @constructor
 * @typedef {Object} CommentThing
 * @member {Meta} CommentThing.meta
 * @member {Activity} CommentThing.activity
 * @member {Scores} CommentThing.scores
 * @member {Author} CommentThing.author
 * @member {Flags} CommentThing.flags
 * @member {Subreddit} CommentThing.subreddit
 * @member {Times} CommentThing.times
 * @member {Mod} CommentThing.mod
 * @member {Content} CommentThing.contents
 * @member {View} CommentThing.view
 */
module.exports = function (rawThing) {
  this.raw = () => rawThing
  this.raw.helperFunc = true

  this.meta = newCall(CommentThing.prototype.Meta, rawThing)
  this.activity = newCall(CommentThing.prototype.Activity, rawThing)
  this.scores = newCall(CommentThing.prototype.Scores, rawThing)
  this.author = newCall(CommentThing.prototype.Author, rawThing)
  this.flags = newCall(CommentThing.prototype.Flags, rawThing)
  this.subreddit = newCall(CommentThing.prototype.Subreddit, rawThing)
  this.times = newCall(CommentThing.prototype.Times, rawThing)
  this.mod = newCall(CommentThing.prototype.Mod, rawThing)
  this.contents = newCall(CommentThing.prototype.Contents, rawThing)
  this.view = newCall(CommentThing.prototype.View, rawThing)
}
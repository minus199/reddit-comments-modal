const _ = require('lodash')

/**
 * @constructor
 * @typedef {Object} MediaEmbed
 */
function MediaEmbed (content, width, height, scrolling) { //"media_embed"
  this.contents = content // html entities string
  this.width = width
  this.height = height
  this.scrolling = scrolling
}



function Preview (enabled, images) { //"preview"
  this.enabled = enabled

  this.images = _.map(images, image => {
    const source = new Preview.Image('source', image.source)
    const resolutions = _.map(image.resolutions, resolution => new Preview.Image('resolutions', resolution))

    return {source, resolutions, id: image.id, variants: image.variants}
  })
}

Preview.Image = function (type, {url, width, height}) {
  this.type = type
  this.url = url
  this.width = width
  this.height = height
}

function SecureMediaEmbed(height, media_domain_url, scrolling, width,content ){ //secure_media_embed
  this.contents = content
  this.width = width
  this.scrolling = scrolling
  this.mediaDomainUrl = media_domain_url
  this.height = height
}

function Media(){
  this.type = '' //enum? like youtube.com
  this.providerUrl = oembed.provider_url
  this.title = oembed.title
  this.type = oembed.type
  this.html = oembed.html
  this.thumbnailWidth = oembed.thumbnail_width
  this.height = oembed.height
  this.width = oembed.width
  this.version = oembed.version
  this.authorName = oembed.author_name
  this.providerName = oembed.provider_name
  this.thumbnailUrl = oembed.thumbnail_url
  this.thumbnailHeight = oembed.thumbnail_height
  this.authorUrl = oembed.author_url
}

// function SecureMedia (type, oembed) { //"secure_media" Extends media
const _ = {get: require('lodash/get')}

const {RootIndicaLeaf, CommentIndicaLeaf} = require("indica/core/modal/IndicaLeaf"),
  CommentThing = require('./types/CommentThing'),
  RootThing = require('./types/RootThing')

const Builder = module.exports.Builder = function ThingBundleBuilder (rawThing) {
  this.rawRoot = _.get(rawThing, '[0].data.children[0].data', null)
}

Builder.prototype = {
  createRoot () {
    const rootLeaf = new RootIndicaLeaf()
    Object.assign(rootLeaf, new RootThing(this.rawRoot))
    return rootLeaf
  },

  extractCreateLeaf (rawLeaf) {
    const reps = _.get(rawLeaf, 'replies.data.children', []).map(c => this.recurseReplies(c, false))
    const currentLeaf = new CommentIndicaLeaf(reps)
    Object.assign(currentLeaf, new CommentThing(rawLeaf))
    return currentLeaf
  },

  createMoreIndicator (data) {
    return $(`<div class="more-replies-indicator" data-thing-id="${data.children.join(',')}"></div>`)
  },

  recurseReplies (rawThing, isMore = false) {
    const {data, kind} = rawThing

    if (kind === 'Listing') return data.children.map(c => this.recurseReplies(c, isMore))
    if (kind === 'more') return {
      extractIndicaOpts: () => {{}},
      toTreeNode: () => this.createMoreIndicator(data),
      kind: 'more',
    }
    if (kind === 't1') return this.extractCreateLeaf(data)

    console.error(`Kind ${kind} is unhandled.`)
  },

  build (rawThings) {
    return {
      rootLeaf: this.createRoot(),
      thingIndicaLeafs: this.recurseReplies(rawThings),
    }
  },
}

module.exports = {
  builder: (rawThing) => (new Builder(rawThing)).build(rawThing[1]),
}


console.info(Date.now(), 'Booting the indica...')
window.$ = window.jQuery = require('jquery')
require('jqueryui')
require('bootstrap')
require('timeago')
require('indica/aux/indica-templating')

module.exports = {
	/**
	 * {module.MainIndicaContainer}
	 */
	app: require('indica/core/modal')
}
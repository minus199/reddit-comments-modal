const _ = require('lodash')

/**
 *
 * @type {module.IndicaTemplatingCompiler}
 */
class IndicaTemplatingCompiler {
	
	static replaceVar(varName) {
		return new RegExp(`{{([${varName}^}}]+)}}`, 'g')
	}
	
	static replaceAll() {
		return new RegExp(`{{([^(}})]+)}}`, 'g')
	}
	
	isInstalled() {
		return String.prototype.indicaTemplating === this.compile
	}
	
	extractAll(template) {
		const matches = [], matchesTrimmed = []
		template.replace(/{{([^(}})]+)}}/g, function (g0, g1) {
			matches.push(g0)
			matchesTrimmed.push(g1)
		})
		
		return matchesTrimmed.reduce((acc, match, index) => {
			const [varName, ...commands] = match.split(':').reverse()
			acc[varName] = {commands, match: `{{${match}}}`}
			return acc
		}, {})
	}
	
	/**
	 * @param opts
	 * @param template
	 * @param replaceOnMissing
	 * @returns {*}
	 * opts are key value store of varName to be replaced with var value {varName: varValue}
	 * replaceOnMissing will put this value in case the value of the var cannot be extracted from opts
	 **/
	compile(opts = {}, template = '', replaceOnMissing = '') {
		return _.reduce(this.extractAll(template), (acc, {commands, match}, varName) => {
			const numOptsKeys = Object.keys(opts).length
			const isKeySet = varName in opts
			if (numOptsKeys > 0 && isKeySet) {
				const replacementValue = opts[varName] === undefined ? replaceOnMissing : opts[varName]
				const computed = _.map(commands, c => COMMANDS[c]).filter(c => !!c).reduce((acc, current) => current(acc), replacementValue)
				return acc.replace(IndicaTemplatingCompiler.replaceVar(match.escapeRegex()), computed)
			}
			
			// If opts is an object; And was populated(given number of keys that their value is not undefined bigger than 0);
			// And the current key is expected by the template; And the current key is missing from opts;
			// console the error
			if (numOptsKeys > 0 && !isKeySet) console.error(`opts missing ${varName}`, opts)
			
			return acc.replace(IndicaTemplatingCompiler.replaceVar(match.escapeRegex()), replaceOnMissing)
		}, template)
	}
	
	install() {
		if (this.isInstalled()) {
			console.info('indicaTemplating is already installed.')
			return
		}
		
		this.install = () => {}
		this.isInstalled = () => true
		
		
		String.prototype.escapeRegex = function () {
			return this.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/gm, '\\$&')
		}
		
		const compiler = this.compile.bind(this)
		String.prototype.indicaTemplating = function (opts) {
			return compiler(opts, this.toString())
		}
		
		console.info('indicaTemplating was installed.')
	}
}

const instance = new IndicaTemplatingCompiler()
instance.install()

/**
 * Not really needed as it's exposed via String.prototype
 * @param opts
 * @param template
 */
module.exports.compile = (opts, template) => instance.compile(opts, template)
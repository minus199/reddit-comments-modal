module.exports = function newCall (Cls, rawThing) {
  const extractedArgs = Cls.rawKeyNames.reduce((acc, k) => {
    if (Object.keys(rawThing).includes(k)) {
      if (k === 'replies') {
        // const children = _.get(rawThing, 'replies.data.children')
        // recurse
      }

      acc.boundArgs.push(rawThing[k])
      acc.dictArgs.push({name: k, val: rawThing[k]})
      return acc
    }

    acc.errors.push({name: k, val: undefined, msg: `No key ${k} found in comment thing`, rawThing, keys: Cls.rawKeyNames})

    return acc
  }, {boundArgs: [null], dictArgs: [], errors: []})

  const Constructor = Function.prototype.bind.apply(Cls, extractedArgs.boundArgs)
  return new Constructor()
}
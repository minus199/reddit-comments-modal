const fs = require('fs')
const _ = require('lodash')
const {RootThingDesc, ThingDesc} = require("indica/core/entities/thing/descriptors")
const NEW_CALL = require("indica/aux/code-gen/newCall").toString()
module.exports = {
  /**
   *
   * @param descriptor
   * @return {Promise.<string>}
   */
  codeGenerator (descriptor, rootTypeName) {
    const dict = _.reduce(descriptor, (acc, {attrName, section, html = false}) => {
      return _.set(acc, [section, attrName], {attrName, section, html})
    }, {})

    return _.reduce(dict, (acc, sectionAttributes, sectionName) => {
        const TypeFullyQualifiedName = _.upperFirst(_.camelCase(sectionName))
        acc.knownTypes.push(TypeFullyQualifiedName)
        const attributes = Object.keys(sectionAttributes)
        const htmlAttributes = _.map(_.filter(sectionAttributes, attr => attr.html), x => x.attrName)

        /* Docs */
        const members = attributes.map(attributeName => `* @member ${TypeFullyQualifiedName}.${_.camelCase(attributeName)}`)
        const docs = ['/**', '* @constructor', `* @typedef {Object} ${TypeFullyQualifiedName}`, members, '*/']

        /* Constructor */
        const _constructor = _.flatten([
          `module.exports.${TypeFullyQualifiedName} = function (${attributes.join(', ')}) {`,
          `this.typeName = '${TypeFullyQualifiedName}'`,
          `this.attributes = {${attributes.map(attributeName => `\n${_.camelCase(attributeName)}: ${attributeName}`)}}`,
          '}', // module.exports

          `module.exports.${TypeFullyQualifiedName}.rawKeyNames = ['${attributes.join('\', \'')}']`,
          `module.exports.${TypeFullyQualifiedName}.displayAttributes = [${htmlAttributes.length === 0 ? '' : `'${htmlAttributes.join('\', \'')}'`}]`,
          '\n',
        ])

        acc.normalized[sectionName] = {
          split: {docs, _constructor},
          asString: [docs, _constructor].map(p => p.join('\n')).join('\n'),
          builderLine: `this.${_.camelCase(sectionName)} = newCall(module.exports.${TypeFullyQualifiedName}, rawThing)`,
          constructorDocLine: `* @member {${TypeFullyQualifiedName}} ${rootTypeName}.${_.camelCase(sectionName)} `,
        }

        return acc
      }
      , {knownTypes: [], normalized: {}})
  },
  buffer (writableStream, type, typeName) {
    _.each(type.normalized, _type => writableStream.write(_type.asString + '\n'))

    writableStream.write(`\n${NEW_CALL}\n`)

    const docs = ['/**', '* @constructor', `* @typedef {Object} %s', ${typeName}`].join('\n')
    writableStream.write(docs + '\n')
    _.each(type.normalized, _type => writableStream.write(_type.constructorDocLine + '\n'))
    writableStream.write('*/\n')

    writableStream.write(`module.exports.${typeName} = function (rawThing){\n`)
    writableStream.write('this.raw = rawThing\n')
    _.each(type.normalized, _type => writableStream.write(_type.builderLine + '\n'))
    writableStream.write('}')

    return writableStream
  },

  generateTypes () {
    const rootType = this.codeGenerator(RootThingDesc, 'RootThing')
    const rootEntityFileName = `/tmp/genOutput/RootThing.js`
    const rootOutputFileStream = fs.createWriteStream(rootEntityFileName)
    this.buffer(rootOutputFileStream, rootType, 'RootThing')

    const commentThingType = this.codeGenerator(ThingDesc, 'CommentThing')
    const commentsEntityFileName = `/tmp/genOutput/Things.js`
    const commentsOutputFileStream = fs.createWriteStream(commentsEntityFileName)
    this.buffer(commentsOutputFileStream, commentThingType, 'CommentThing')
  },
}

require("indica/aux/utils.codGen").generateTypes()




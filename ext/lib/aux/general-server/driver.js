
const sw = require('snoowrap')
const snoowrap = new sw({
  userAgent: 'made with love by /u/im-root',
  clientId: '3lmB1FpGwD-YUQ',
  clientSecret: 'EgBEuzWpX7RHr8WCN4zXYbH5WvU',
  refreshToken: 'put your refresh token here',
})

const generateURL = () => {
  const scopes = [
    'identity', 'edit', 'flair', 'history', 'modconfig',
    'modflair', 'modlog', 'modposts', 'modwiki', 'mysubreddits', 'privatemessages',
    'read', 'report', 'save', 'submit', 'subscribe', 'vote', 'wikiedit', 'wikiread'].join(' ')

  const base = 'https://www.reddit.com/api/v1'
  const clientID = '3lmB1FpGwD-YUQ'
  const state = 'RANDOM_STRING'
  const redirectURI = 'http://localhost:8081/auth'
  const duration = ['permanent', 'temporary'][0]

  const requestParams = `response_type=code&state=${state}&redirect_uri=${redirectURI}&duration=${duration}&scope=${scopes}`
  const uri = `${base}/authorize?client_id=${clientID}&${requestParams}`

  return uri
}


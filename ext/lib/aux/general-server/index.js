const restify = require('restify')
const server = restify.createServer()

server.use(restify.plugins.bodyParser())
server.use(restify.plugins.acceptParser(server.acceptable))
server.use(restify.plugins.queryParser({allowDots: true}))
server.use(restify.plugins.jsonp())

server.get('/', respond)
server.get('/auth', respond)
server.post('/auth', respond)

server.listen(8081, function () {
  console.log('%s listening at %s', server.name, server.url)
})

function respond (req, res, next) {
  const {code,state} = req.query
  res.send('..')
  next()
}

server.get(/^\/([a-zA-Z0-9_\.~-]+)\/(.*)/, function (req, res, next) {
  console.log(req.params[0])
  console.log(req.params[1])
  res.send(200)
  return next()
})

/*

server.get('/', function(req, res, next) {
  res.send('home')
  return next();
});

server.post('/foo',
  function(req, res, next) {
    req.someData = 'foo';
    return next();
  },
  function(req, res, next) {
    res.send(req.someData);
    return next();
  }
);
// dedupe slashes in URL before routing
server.pre(restify.plugins.dedupeSlashes());
*/

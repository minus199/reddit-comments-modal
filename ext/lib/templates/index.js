const dust = require('dustjs-linkedin')
const htmlEntities = new (require('html-entities').AllHtmlEntities)()

Object.assign(dust.filters, {
	// These filters will be available via the template to be applied on a specific var
	// Example, convert date to some format
	htmlDecode: function (value) {
		return typeof value === 'string' ? htmlEntities.decode(value) : value
	},
	dateISO: function (value) {
		return new Date(Number.parseInt(value) * 1000).toISOString()
	}
})

const render = (templateName, opts = {}) => new Promise((rez, rej) => {
	console.debug(`Rendering ${templateName}`)
	
	opts[templateName] = opts
	dust.render(templateName, opts, (err, out) => {
		err ? rej(err) : rez($(out))
	}).catch(err => console.error("Rendering error", err))
})

module.exports = {
	/**
	 *
	 * @param opts
	 * @return {Promise}
	 */
	root(opts) {
		return render('root', opts)
	},
	/**
	 *
	 * @param opts
	 * @return {Promise}
	 */
	leaf(opts) {
		return render('leaf', opts)
	},
	/**
	 *
	 * @param opts
	 * @return {Promise}
	 */
	indica(opts) {
		return render('indica', opts)
	},
	/**
	 *
	 * @param opts
	 * @return {Promise}
	 */
	metrics(opts) {
		return render('metrics', opts)
	}
}
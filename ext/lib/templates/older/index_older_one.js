const _ = require('lodash')

const renderSection = (section, opts) => _.mapValues(section, template => $(template.indicaTemplating(opts)))
const resolved = require('fs').readFileSync(require('path').resolve(__dirname, './templates.html'), 'utf8').

  split(/\n*#indica-delimiter\n*/).

  map(k => $(k))

console.log('')

const Templates = require('./templates.js')

module.exports = {
  $container (asJQuery = true) {
    const {container, containerToggle, commentsTree, sideBar} = renderSection(Templates.mainIndicaContainer)
    const children = [sideBar.append(containerToggle), module.exports.$root(), commentsTree] // By appending order
    const $container = $(container).append(children)

    return asJQuery ? $container : $container[0]
  },

  /**
   * @param opts {{
   * title: {titleLink, titleText},
   * metrics,
   * meta: {authorLink: string, authorName, subredditURL: string, subredditName: string, createdDate: number},
   * contents: {html},
   * preview: {thumbnail: string, thingURL, srcOrig: string, hrefTN: string, thumbnailSrcURI: string},
   * expanded
   * }}
   * @return {jQuery}
   */
  $root (opts = {}) {
    const root = Templates.root
    const [meta, title, expanded, thumbnail, contents] = ['meta', 'title', 'expanded', 'thumbnail', 'contents'].
      map(k => $(root[k]/*.indicaTemplating(opts[k])*/))

    const header = $(root.header).prepend(meta).append(title)

    return $(root.wrapper).
      append(module.exports.$metrics(opts['metrics']).addClass('root-metrics')).
      append(thumbnail).
      append(header).
      append(expanded).
      append(contents)
  },

  /**
   * @param opts {{
   *  scores: {score, ups, downs, upvoteRatio, numComments, viewCount, numCrossposts, likes, gilded},
   *  meta: {authorLink, authorName, subredditURL, subredditName, createdDate}
   * }}
   *
   * @return {jQuery}
   */
  $replyThing (opts) {
    const $metrics = module.exports.$metrics(opts['metrics']).addClass('comment-metrics')
    return $(Templates.replyThing/*.indicaTemplating(opts)*/).prepend($metrics)
  },

  $metrics (opts) {
    return $(Templates.metrics/*.indicaTemplating(opts)*/)
  },
}


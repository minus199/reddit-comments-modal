const Templates = module.exports = {}

Templates.mainIndicaContainer = {
  container: `<div id="indica" class="container">`,
  commentsTree: `<div id="indica-comments-tree"></div>`,
  sideBar: `<div id="indica-side-bar-menu"></div>`,
  containerToggle: `<button type="button" id="indica-toggle-root" class="btn btn-default">X</button>`,
}

Templates.metrics = `<div class="metrics" >
              <div class="arrow login-required access-required upmod" data-event-action="upvote" role="button" aria-label="upvote"></div>
              <div class="score dislikes" title="{{downs}}">{{downs}}</div>
              <div class="score unvoted" title="{{ups}}">{{ups}}</div>
              <div class="score likes" title="{{ups}}">{{ups}}</div>
              <div class="arrow down login-required access-required" data-event-action="downvote" role="button" aria-label="downvote"></div>
          </div>`

Templates.root = {
  wrapper: `<div id="indica-tree-root" class="container"></div>`,

  header: `<div id="indica-root-header"></div>`,

  title: `<span  id="indica-root-title-container">
            <a id="root-title-link" href="{{titleLink}}"><h1 class="display-3" id="modal-title">{{titleText}}</h1></a>
            <a href="{{thingURL}}" id="root-comments-link" target="_blank">Open in tab</a>
        </span>`,

  meta: `<span id="root-meta-data-container">
            <span>Submitted to<a id="root-subreddit" class="root-subreddit subreddit hover" href="{{subredditURL}}">{{subredditName}}</a></span>
            <span>by<a id="thing-author root-author" class="author may-blank thing-author" href="{{authorLink}}">{{authorName}}</a></span>
            <span><time id="thing-created root-created indica-thing-date" class="timeago" datetime="{{date:createdDate}}">{{date:createdDate}}</time></span>
         </span>`,

  thumbnail: `<div id="root-tn-container">
                <a id="root-thumbnail-link" class="root-thumbnail-link">
                    <img id="root-thing-tn" class="thing-tn" src="{{thumbnailURL}}">    
                </a>
            </div>`,

  contents: `<div id="root-decoded-contents">{{decode:html}}</div>`,

  expanded: `<div id="root-media" data-root-media-provider-type="{{mediaType}}">
                <div id="root-media-meta">
                    <div id="root-media-provider-name" data-root-media-provider-typ="{{providerType}}">
                      <a href="{{providerUrl}}">{{providerName}}</a>
                    </div>
                    <div id="root-media-title">{{title}}</div>
                    <div id="root-media-author"><a href="{{authorUrl}}">{{authorName}}</a></div>
                </div>
                <div id="root-content-preview">
                    <div id="root-content-preview-tn"><img src="{{thumbnailUrl}}"/></div>
                    <div id="root-content-preview-fullsize">{{decode:html}}</div>
                </div>
            </div>`,

}

Templates.replyThing = `<div class='indica-comment-content-entry'>
                            <div class='thing-meta-data'>
                                <span class='thing-author comment-author'><a href="{{authorURL}}">{{authorName}}</a></span> 
                                <span>
                                    <time class="thing-created comment-thing-created indica-thing-date" datetime="{{date:createdDate}}">{{date:createdDate}}</time>
                                </span>
                            </div>
                            {{decode:contents}}
                            <div id="replies"/>
                        </div>`
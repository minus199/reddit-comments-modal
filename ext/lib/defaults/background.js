dust = require('dustjs-linkedin')


function handleMessage(request, sender, sendResponse) {
	console.info('Got start script message')
	console.table({request, sender, tabId: sender.tab.id, file: 'templates.min.js'})
	
	//{file: browser.extension.getURL('templates.min.js')}
	browser.tabs.executeScript(sender.tab.id, {file: 'templates.min.js'})
		.then(templates => {
			console.table({meta: 'Executed templates', templates})
			browser.tabs.sendMessage({templates})
		}).catch(error => console.log('error from loading template:', error))
}


browser.runtime.onMessage.addListener(handleMessage)


browser.runtime.onInstalled.addListener(details => {
	/*
		browser.notifications.create('onInstalled', {
			title: `Runtime Examples version: ${browser.runtime.getManifest().version}`,
			message: `onInstalled has been called, background page loaded`,
			type: 'basic',
			isInstall: details.reason === 'install',
			isUpdate: details.reason === 'update'
		})
	*/
	
	/*browser.tabs.getCurrent().then((...tab) => {
		console.table(tab)
		console.table({
			meta: `${Date.now()} [${__filename}}] Extension Indica will be installed`,
			details,
			tab,
			tabID: tab.id, file: browser.extension.getURL('templates.min.js')
		})
		
		browser.tabs.executeScript(tab.id, {file: browser.extension.getURL('templates.min.js')})
			.then(templates => {
				console.table({meta: 'Executed templates', templates})
				browser.tabs.sendMessage({templates})
			}).catch(error => console.log('error from loading template:', error))
		console.info(`${Date.now()} [${__filename}}]`, 'Extension Indica was installed', details)
	})*/
})

browser.runtime.onConnect.addListener(contentScriptPort => {
	console.log('on connect')
})

/*
browser.browserAction.onClicked.addListener(() => {

})
*/

function installTemplates(tabs) {
	
	browser.tabs.query({currentWindow: true, active: true}).then(installTemplates).catch(onError)
	// tabs.forEach(tab => browser.tabs.sendMessage(tab.id, {promisedTemplates: exec(tab.id, {exec: true})})
}

function loadTemplates() {
	const getWebAccessibleResources = () => {
		const executor = file => browser.tabs.executeScript({file})
		
		const webAccessibleResources = browser.runtime.getManifest().web_accessible_resources
		console.table(webAccessibleResources)
		
		return Promise.all(webAccessibleResources.map(war => {
			return {ex: executor(war), file: war}
		})).then(executors => {
			console.table(executors)
			console.table(executors.reduce((acc, {ex, file}) => {
				acc[file] = ex
				return acc
			}, {}))
		})
	}
	
	console.info('loading resources')
	try {
		return getWebAccessibleResources()
	} catch (err) {
		console.error('unable to load resources', err)
		return Promise.reject(err)
	}
}

///////////////////////////////////
function fetchPreview(url, resolve, reject) {
	const API_KEY = '59eb52a67c72e528504bd4fce78dd2d69ce3eb5298f11'
	
	const httpRequest = new XMLHttpRequest()
	
	function onResponse(e) {
		if (httpRequest.readyState === XMLHttpRequest.DONE) {
			if (httpRequest.status === 200) return resolve({payload: JSON.parse(httpRequest.responseText), e})
			
			console.error('>'.repeat(10), httpRequest, e)
			return reject({httpRequest, e})
		}
	}
	
	httpRequest.onreadystatechange = onResponse
	httpRequest.open('GET', `http://api.linkpreview.net/?key=${API_KEY}&q=${url}`, true)
	httpRequest.send()
}

// new Promise((resolve, reject) => fetchPreview('https://www.google.com/', resolve, reject)).then(console.log)


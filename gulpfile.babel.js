const {MAIN_DIST_DIR, ENTRY, DIST, TARGETS, LIB_DIR} = require('./gulpConfig')
const path = require('path'),
	gulp = require('gulp'),
	fs = require('fs'),
	$ = require('gulp-load-plugins')(),
	gulpif = require('gulp-if'),
	dustGulp = require('gulp-dust'),
	dust = require('dustjs-linkedin'),
	{Readable} = require('stream')

const runSeq = require('run-sequence'),
	browserify = require('gulp-browserify'),
	concatCss = require('gulp-concat-css'),
	cleanCSS = require('gulp-clean-css'),
	es = require('event-stream'),
	concat = require('gulp-concat')

/** Copy resources **/
const createCopyTask = ({from, to, rename}) => rename
	? gulp.src(from).pipe($.rename(rename)).pipe(gulp.dest(to))
	: gulp.src(from).pipe(gulp.dest(to))

gulp.task('copy-fonts', done => createCopyTask(DIST.fonts))
gulp.task('copy-images', done => createCopyTask(DIST.images))
gulp.task('copy-html', done => createCopyTask(DIST.html))
gulp.task('copy-locales', done => createCopyTask(DIST._locales))
gulp.task('copy', ['copy-fonts', 'copy-locales', 'copy-html'])
/** Copy resources END **/

/** Compile Start **/

gulp.task('compile-styles', () => {
	const pathToOutputDirectory = path.basename(TARGETS.BUNDLE_CSS)
	return gulp.src(DIST.styles.from).pipe(concatCss(pathToOutputDirectory, {rebaseUrls: false})).pipe(cleanCSS({rebase: false})).pipe(gulp.dest(path.dirname(TARGETS.BUNDLE_CSS)))
})

gulp.task('compile-sprite', function () {
	
	const sprity = require('sprity')
	
	sprity.src({
		src: DIST.images.from,
		style: 'sprite.css',
		base64: true
		// processor: 'sass', // make sure you have installed sprity-sass
	}).pipe(gulpif('*.png', gulp.dest(DIST.images.to), gulp.dest(DIST.styles.to)))
})

gulp.task('compile-buttons-css', function () {
	function ImageSizeCalculator() {
		const
			pngSignature = 'PNG\r\n\x1a\n',
			pngImageHeaderChunkName = 'IHDR',
			pngFriedChunkName = 'CgBI'
		
		this.detect = function detect(buffer) {
			if (pngSignature === buffer.toString('ascii', 1, 8)) {
				
				let chunkName = buffer.toString('ascii', 12, 16)
				
				if (chunkName === pngFriedChunkName) {
					chunkName = buffer.toString('ascii', 28, 32)
				}
				
				if (chunkName !== pngImageHeaderChunkName) {
					throw new TypeError('invalid png')
				}
				
				return chunkName
			}
		}
		
		this.calculate = function calculate(buffer) {
			if (buffer.toString('ascii', 12, 16) === pngFriedChunkName) {
				return {
					width: buffer.readUInt32BE(32),
					height: buffer.readUInt32BE(36)
				}
			}
			
			return {
				width: buffer.readUInt32BE(16),
				height: buffer.readUInt32BE(20)
			}
		}
	}
	
	const imageSizeCalculator = new ImageSizeCalculator()
	
	const vfs = require('vinyl-fs'), map = require('map-stream'), concat = require('vinyl-fs-concat'),
		es = require('event-stream')
	
	const handleImage = function (imgToSprite, cb) {
		const {width, height} = imageSizeCalculator.calculate(imgToSprite.contents)
		const base64Encoded = imgToSprite.contents.toString('base64')
		
		const currentButton = [
			`.${require('lodash/camelCase')(path.basename(imgToSprite.path))}{`,
			`\tbackground: url(data:image/png;base64,${base64Encoded}) no-repeat left center;`,
			`\tpadding: 5px 0 5px 25px;`,
			`\twidth: ${width}px;`,
			`\theight: ${height}px;`,
			'}\n'
		].join('\n')
		
		imgToSprite.contents = Buffer.from(currentButton, 'utf8')
		cb(null, imgToSprite)
	}
	
	gulp.src(['./ext/images/sprite/*.png']).pipe(map(handleImage)).pipe(concat('buttons.css')).pipe(vfs.dest(`./ext/styles`))
})

gulp.task('compile-buttons-js', function () {
	const readStream = fs.createReadStream(DIST.styles.from + '/buttons.css', {flags: 'r'})
	readStream.on('end', () => {
		writeStream.write('}')
		writeStream.write('const getIcon = name => $(\'<span/>\').addClass(icons[name])\n')
		writeStream.write('module.exports = {getIcon}')
	})
	
	const writeStream = fs.createWriteStream(LIB_DIR + '/aux/buttons.js')
	writeStream.write('const buttons = module.exports = {\n')
	
	readStream.pipe(es.split()).pipe(es.map(function (data, cb) {
		if (!data.startsWith('.')) return cb()
		
		const className = data.split('{')[0].replace(/^\./i, '')
		cb(null, `\t${className}: "${className}",\n`)
	})).pipe(writeStream)
})

gulp.task('compile-buttons', ['compile-buttons-css', 'compile-buttons-js'])

gulp.task('compile-js', ['compile-templates'], function () {
	return gulp.src(ENTRY).pipe(browserify({
		insertGlobals: true,
		debug: true
	})).pipe(gulpif('*content*', $.rename(path.basename(TARGETS.BUNDLE_EXT)), $.rename('background.min.js'))).// pipe(gulpif('*content*', gulp.dest($.rename(path.basename(TARGETS.BUNDLE_EXT))), gulp.dest(path.dirname(TARGETS.BUNDLE_EXT))))
	// pipe(gulpif('*content*', gulp.dest(DIST.images.to), gulp.dest(DIST.styles.to)))
	// pipe($.rename(path.basename(TARGETS.BUNDLE_EXT))).
	pipe(gulp.dest(path.dirname(TARGETS.BUNDLE_EXT)))
})

gulp.task('compile-templates', done => {
	const outputStream = fs.createWriteStream(DIST.templates.to)
	fs.readdirSync(DIST.templates.from).forEach(raw_template => {
		const raw = fs.readFileSync(path.resolve(DIST.templates.from, raw_template), 'utf8')
		const compiled = dust.compile(raw, raw_template.split('.')[0])
		
		outputStream.write(`${compiled}\n`)
	})
	outputStream.end(null)
	done()
})
/** Compile END **/

gulp.task('make-manifest', () => {
	const manifest = require(DIST.manifest.from)
	const s = fs.createWriteStream(DIST.manifest.to + '/manifest.json')
	s.write(JSON.stringify(manifest))
	return s
})

/** Sequences **/
gulp.task('clean', function () {
	return gulp.src([MAIN_DIST_DIR]).pipe($.clean({read: false, force: true}))
})

gulp.task('bundle', function (done) {
	require('mkdirp')(MAIN_DIST_DIR)
	runSeq('compile-js', 'compile-styles', 'copy', done)
})

gulp.task('bundle.firefox', [], function (done) {
	runSeq('clean', 'bundle', 'make-manifest', function () {
		done()
	})
})

gulp.task('bundle.chrome', ['bundle'], function () {
	return runSeq('compile-js', 'copy')
})

// const prodSeq = runSeq('build', 'copy', 'wrap', 'package', callback)

gulp.task('wrap', function () {
	return gulp.src([TARGETS.BUNDLE_VENDOR, TARGETS.BUNDLE_EXT]).pipe($.rename(path.basename(TARGETS.BUNDLE_WRAPPED))).pipe(gulp.dest(path.dirname(TARGETS.BUNDLE_WRAPPED)))
})

gulp.task('package', function () {
	return gulp.src(['!' + MAIN_DIST_DIR + '/*.min*.js*', MAIN_DIST_DIR + '/**/*.*']).pipe($.zip('packaged.zip')).pipe(gulp.dest('package'))
})

gulp.task('default', ['bundle-firefox'])

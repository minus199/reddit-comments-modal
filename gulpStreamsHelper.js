const through = require('through')
const Decoder = require('string_decoder').StringDecoder

module.exports = function split (matcher, mapper, options) {
  const decoder = new Decoder()

  let soFar = ''
  const maxLength = options && options.maxLength

  if ('function' === typeof matcher)
    mapper = matcher, matcher = null

  if (!matcher)
    matcher = /\r?\n/

  function emit (stream, piece) {
    if (mapper) {
      try {
        piece = mapper(piece)
      }

      catch (err) {
        return stream.emit('error', err)
      }

      if ('undefined' !== typeof piece)
        stream.queue(piece)
    }

    else
      stream.queue(piece)
  }

  function next (stream, buffer) {
    const pieces = ((soFar != null ? soFar : '') + buffer).split(matcher)
    soFar = pieces.pop()

    if (maxLength && soFar.length > maxLength)
      stream.emit('error', new Error('maximum buffer reached'))

    for (let i = 0; i < pieces.length; i++) {
      const piece = pieces[i]
      emit(stream, piece)
    }
  }

  return through(function (b) {
      next(this, decoder.write(b))
    },
    function () {
      if (decoder.end)
        next(this, decoder.end())
      if (soFar != null)
        emit(this, soFar)
      this.queue(null)
    })
}


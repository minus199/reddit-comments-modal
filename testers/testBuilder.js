const path = require('path')
const _ = {reduce: require('lodash/reduce')}
const $ = require('cheerio')


require('indica/aux/indica-templating')


const rootTemplate = require('indica/templates').root
const ThingBuilder = require('indica/core/entities/thing').builder
const IndicaDataProvider = require('indica/core/modal/IndicaDataProvider').file
const indicaDataProvider = new IndicaDataProvider()

const filename = '../test/resources/res/last_week_bill_murray_went_to_a_bluegrass_concert.json'
const url = path.resolve(__dirname, filename)
// const url = "https://www.reddit.com/r/videos/comments/7byflu/7_years_later_everything_antoine_said_about.json"

const start = module.exports.output = Date.now()
const b = module.exports.builder = () => {
	indicaDataProvider.provide(url).then(ThingBuilder).then(leaves => {
		const {rootLeaf, thingIndicaLeafs} = leaves
		/*const red = _.reduce(rootLeaf.extractIndicaOpts().expanded.oembed, function (acc, currentVal, currentKey) {
		  const [keyNS, keyId] = currentKey.split('_')
		  acc[keyNS] = acc[keyNS] || {htmlContainer: $('<ul/>')}
	
		  if (keyId === undefined) {
			acc[keyNS].val = currentVal
		  } else {
			acc[keyNS][keyId] = {val: currentVal}
		  }
	
		  const k = $('<li/>').attr('id', `key-${currentKey}`).addClass('root-keys').text(currentKey)
		  const v = $('<li/>').attr('id', `value-${currentKey}`).addClass('root-values').text(currentVal)
		  const html = $('<ul/>').attr('class', 'root-data-entry').append([k, v])
		  acc[keyNS].htmlContainer.append($('<li/>').append(html))
	
		  return acc
		}, {})*/
		
		// const mainHTML = _.reduce(red, (acc, section, sectionName) => acc.append($('<li/>').text(sectionName).append(section.htmlContainer)), $('<ul>'))
		// const onlyHTML = $("<foo/>").append(mainHTML).html()
		const rootContainer = rootTemplate(rootLeaf.extractIndicaOpts())
		
		const htmlContainer = $('<indicaTree/>').append(thingIndicaLeafs.map(leaf => leaf.toTreeNode()))
		const _html = $('<foo/>').append(htmlContainer).html()
		
		const elapsed = Date.now() - start
		const rootHTML = $('<foo/>').append(rootContainer).html()
		const leafHTML = $('<foo/>').append(thingIndicaLeafs[0].toTreeNode()).html()
		
		console.log({elapsed, rootLeaf, thingIndicaLeafs})
		return module.exports.output = {elapsed, rootLeaf, thingIndicaLeafs}
	}).catch(err => {
		console.error(err)
		module.exports.output = {err: err}
	})
}

b()
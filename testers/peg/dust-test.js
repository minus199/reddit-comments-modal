const dust = require('dustjs-linkedin'), path = require('path'),
	fs = require('fs'), {Readable} = require('stream'),
	peg = require('pegjs')


const rawFiles = {
	parser: fs.readFileSync('./dust.pegjs', 'utf8'),
	fooTemplate: fs.readFileSync('./template.dust', 'utf8'),
	wrapper: fs.readFileSync('./wrapper.txt', 'utf8')
}

// dust.compile(rawFiles.fooTemplate, 'foo')

const isRuntime = false
const parserSource = peg.generate(rawFiles.parser, {
	exportVar: 'aaa',
	format: 'commonjs',
	output: isRuntime ? 'parser' : 'source'
})
fs.writeFileSync('./genParser.js', parserSource, 'utf8')
const parser = isRuntime ? parserSource : require('./genParser')

const c = parser.parse(rawFiles.fooTemplate)


//
// const [wrapStart, wrapEnd] = rawFiles.wrapper.replace('@@build', 'DATADADA').split('@@parser')
// const parser = `${wrapStart}\n\n${parserSource}\n\n${wrapEnd}`
// fs.writeFileSync('./genParser.js', parser, 'utf8')


const type = c.shift()

const d = c.reduce((acc, current) => {
	const [type, ...vars] = current
	acc[type] = acc[type] || {}
	acc[type] = vars.reduce((_acc, currentVar) => {
		const [varType, varName, ...extras] = currentVar
		
		_acc[varType] = acc[varType] || []
		_acc[varType].push({varName, extras})
		return _acc
	}, {})
	
	return acc
}, {})
const compiledTemplate = dust.compile(rawFiles.fooTemplate, 'foo')


const readableOutputStream = new Readable()
fs.readdirSync('../ext/static/dust').forEach(raw_template => {
	const raw = fs.readFileSync(path.resolve(__dirname, '../ext/static/dust', raw_template), 'utf8')
	const compiled = dust.compile(raw, raw_template)
	dust.loadSource(compiled)
	
	readableOutputStream.push(`${compiled}\n`)
})

dust.render.indica()


fs.readdirSync('./ext/static/dust').forEach(raw_template => {
	const raw = fs.readFileSync(path.resolve(__dirname, '../ext/static/dust', raw_template), 'utf8')
	const compiled = dust.compile(raw, raw_template)
	readableOutputStream.push(`${compiled}\n`)
})
readableOutputStream.push(null)

readableOutputStream.pipe(process.stdout)